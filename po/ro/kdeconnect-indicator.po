# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
# Sergiu Bivol <sergiu@cip.md>, 2020, 2021, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-03 00:41+0000\n"
"PO-Revision-Date: 2024-01-07 13:08+0000\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sergiu Bivol"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sergiu@cip.md"

#: deviceindicator.cpp:53
#, kde-format
msgid "Browse device"
msgstr "Răsfoiește dispozitivul"

#: deviceindicator.cpp:67
#, kde-format
msgid "Send clipboard"
msgstr "Trimite clipboard"

#: deviceindicator.cpp:81
#, kde-format
msgctxt "@action:inmenu play bell sound"
msgid "Ring device"
msgstr "Sună dispozitivul"

#: deviceindicator.cpp:97
#, kde-format
msgid "Send a file/URL"
msgstr "Trimite un fișier/URL"

#: deviceindicator.cpp:107
#, kde-format
msgid "SMS Messages..."
msgstr "Mesaje SMS..."

#: deviceindicator.cpp:120
#, kde-format
msgid "Run command"
msgstr "Rulează comandă"

#: deviceindicator.cpp:122
#, kde-format
msgid "Add commands"
msgstr "Adaugă comenzi"

#: indicatorhelper_mac.cpp:37
#, kde-format
msgid "Launching"
msgstr "Se lansează"

#: indicatorhelper_mac.cpp:87
#, kde-format
msgid "Launching daemon"
msgstr "Se lansează demonul"

#: indicatorhelper_mac.cpp:96
#, kde-format
msgid "Waiting D-Bus"
msgstr "Se așteaptă D-Bus"

#: indicatorhelper_mac.cpp:113 indicatorhelper_mac.cpp:125
#: indicatorhelper_mac.cpp:144
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: indicatorhelper_mac.cpp:114 indicatorhelper_mac.cpp:126
#, kde-format
msgid ""
"Cannot connect to DBus\n"
"KDE Connect will quit"
msgstr ""
"Nu se poate conecta la D-Bus\n"
"KDE Connect se va închide"

#: indicatorhelper_mac.cpp:144
#, kde-format
msgid "Cannot find kdeconnectd"
msgstr "Nu s-a putut găsi kdeconnectd"

#: indicatorhelper_mac.cpp:149
#, kde-format
msgid "Loading modules"
msgstr "Se încarcă module"

#: main.cpp:53
#, kde-format
msgid "KDE Connect Indicator"
msgstr "Indicator KDE Connect"

#: main.cpp:55
#, kde-format
msgid "KDE Connect Indicator tool"
msgstr "Unealtă de indicator KDE Connect"

#: main.cpp:57
#, kde-format
msgid "(C) 2016 Aleix Pol Gonzalez"
msgstr "(C) 2016 Aleix Pol Gonzalez"

#: main.cpp:92
#, kde-format
msgid "Configure..."
msgstr "Configurare..."

#: main.cpp:114
#, kde-format
msgid "Pairing requests"
msgstr "Cereri de asociere"

#: main.cpp:119
#, kde-format
msgctxt "Accept a pairing request"
msgid "Pair"
msgstr "Asociază"

#: main.cpp:120
#, kde-format
msgid "Reject"
msgstr "Refuză"

#: main.cpp:126 main.cpp:136
#, kde-format
msgid "Quit"
msgstr "Termină"

#: main.cpp:155 main.cpp:179
#, kde-format
msgid "%1 device connected"
msgid_plural "%1 devices connected"
msgstr[0] "%1 dispozitiv conectat"
msgstr[1] "%1 dispozitive conectate"
msgstr[2] "%1 de dispozitive conectate"

#: systray_actions/battery_action.cpp:28
#, kde-format
msgid "No Battery"
msgstr "Fără acumulator"

#: systray_actions/battery_action.cpp:30
#, kde-format
msgid "Battery: %1% (Charging)"
msgstr "Acumulator: %1% (se încarcă)"

#: systray_actions/battery_action.cpp:32
#, kde-format
msgid "Battery: %1%"
msgstr "Acumulator: %1%"

#: systray_actions/connectivity_action.cpp:30
#, kde-format
msgctxt ""
"The fallback text to display in case the remote device does not have a "
"cellular connection"
msgid "No Cellular Connectivity"
msgstr "Fără conectivitate celulară"

#: systray_actions/connectivity_action.cpp:33
#, kde-format
msgctxt ""
"Display the cellular connection type and an approximate percentage of signal "
"strength"
msgid "%1  | ~%2%"
msgstr "%1  | ~%2%"

#~ msgid "Get a photo"
#~ msgstr "Ia o poză"

#~ msgid "Select file to send to '%1'"
#~ msgstr "Alegeți fișierul de trimis la „%1”"
